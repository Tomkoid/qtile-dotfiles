#!/usr/bin/env bash 

lxsession &
picom --backend glx --experimental-backends &
setxkbmap cz
volumeicon &
nm-applet &
nitrogen --restore &
xss-lock --transfer-sleep-lock -- i3lock -c 000000 --nofork